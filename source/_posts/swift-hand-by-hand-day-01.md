---
layout: post
title: Swift與iOS APP開發 - 介紹與開發環境安裝
subtitle: ""
date: 2014-07-23 00:29:45
author: Blackie
header-img: ""
categories:
- Mobile
tags:
- Swift
- iOS
---

這篇主要在介紹Swift與iOS的開發環境安裝

<!-- More -->

## Swift介紹

## 安裝步驟

<em> 首先要先確定你有apple developer開發帳號(還沒繳年費的記得快繳錢瞜, 呵呵) </em>

1. 前往 [官方下載](https://developer.apple.com/devcenter/ios/index.action)
2. 登入Developer會員帳號後最上面選擇 iOS 8 Beta

	![01](swift_01_01.png)

3. 拉到下面選擇安裝 Xcode 6 beta

	![02](swift_01_02.png)

4. 下載完dmg檔案後直接雙點安裝然後拖進application資料夾中存放

	![03](swift_01_03.png)

	![04](swift_01_04.png)

5. 按下⌘+R或是上方的執行按鈕執行。可以看到中間區塊的右下角出現 Hello World!字樣。

6. 這樣就完成了我們第一天的學習
