---
title: Using App Scripts to easy create and publish add-ons for Google Sheets
sitemap: true
author: Blackie
date: 2021-09-29 01:10:25
categories:
- JavaScript
tags:
- App Script
- Google Workspace
- Google Slide
---

介紹如何透過 App Scripts 客製化功能、增加生產力

<!-- More -->

![apps_script_logo.png](apps_script_logo.png)

## Google App Scripts

[Google App Scripts](https://developers.google.com/apps-script) 是一個提供能透過客製化程式開發的方式加強 Goolge 既有的雲端商業服務，包含：Calendar, Docs, Drive, Gmail, Sheets 與 Slides 都在支援範圍．

`App Scripts` 目前使用 JavaScripts 作爲唯一的平台開法語言，使用上感覺透過 GCP Cloud Function 的雲端服務，徹底發揮 Serverless 的好處，即是無需程式部署即可透過寫好的程式為 Calendar, Docs, Drive, Gmail, Sheets 與 Slides 開發擴充套件，更大畫的增加生產效能．

當前的 Goolge App Scripts(GAS) 是基於 JavaScript 1.6 但同時包含部分 1.7 與 1.8 所擁有的子集功能，所以大部分 ECMAScript 5 API 是支援的.

## First Google App Scripts with Google Sheets

這邊以 Google Slide 為例，逐一教學建立 Google App Scripts 並展現它的威力幫我們將每筆資料轉成客製化的 JSON 字串．[範例成果](https://docs.google.com/spreadsheets/d/1Vl2ko2uY3BatO09Bh5OmKU7vT858FV8_qNm3GB86y4U/edit?usp=sharing)

1. 首先也是正常的建立一份 Goolge Sheet, 這邊我們填上三個標題欄位: Name, Region 與 Gendar. 接著我們保留一個欄位 `JsonObject`
  ![建立一份新的 Google Sheet](01.png)
2. 而後在 Tools > Script editor 處可以建立一份新的 App Scripts 專案，建立時會預先帶一份 `Code.gs` 這就是我們可以開始寫程式的地方，而預設宣告的 `myFunction` 就是可以給剛剛 Google Sheet 呼叫的函式(function)
  ![建立一份新的 App Scripts 專案](02.png)
  ![修改第一份 Code.gs 的檔案後記得要按下存擋才會生效](03.png)
3. 接著我們開始修改程式碼如下：

  ```js
  function convertToResultJsonStringify(name, region, gendar) {
    let result = {
      "customer":name,
      "live region":region,
      "gendar": gendar === "Female" ? "女性" : "男性"
    }

    return JSON.stringify(result)
  }
  ```

  ![當完成編寫並存擋後原本的 Google Sheet 可以呼叫或是反應更新，無需畫面重整](04.png)
4. 而後將函式於 JsonOject 處貼上名稱後帶入個別要傳入的欄位，這邊要稍微注意載入函示時會一筆筆執行且時快時慢，在慢的時候會短暫出現 Error 的紅框，這邊不一定有錯誤，只要稍等一下即可．
  ![帶入須要傳入的參數](05.png)
  ![等待時會有短暫 Error](06.png)
  ![最終結果](07.png)

上述簡單的四步驟就可以大大的透貴 JS 來幫我們增加生產力，讓時間花在真的值得人事物上！

## Google App Scripts Quota

基本上 GAS 有使用上的限制與不方便之處：

- 運行速度偏慢，。
- 每一個 GAS 程式有執行時間與運行限制，如每次運行的時間有時間限制．最多只有6分鐘. 或是每秒最多只能呼叫執行 30 次.

詳細的限額可以參考 [Quotas for Google Services](https://developers.google.com/apps-script/guides/services/quotas)

![GAS 部分限額資訊](08.png)
