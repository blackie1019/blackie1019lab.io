---
layout: post
title: D3.JS:Intro
subtitle: ""
date: 2014-08-27 00:29:45
author: Blackie
header-img: ""
categories:
- JavaScript
tags:
- D3js
---

[D3.js](http://d3js.org/)是一個JavaScript library，是 Data-Driven Documents只要有資料就可以產生文件，透過本身資料的儲存方式來轉換成圖像的方式呈現，提供不同的視角來檢視你的資料，並從中強化各種視覺呈現的效果與互動．D3.js 可僅透過JavaScript, HTML, SVG and CSS就可以達到圖像化的資料呈現並且可跨瀏覽器支援．

<!-- More -->

## What is D3.js?

現在我們就直接看一個[Live Demo](http://zbryikt.github.io/visualize/dorling/) 感受一下魅力吧!

另外推薦一個台灣人以台灣地圖與資料來呈現空間與數據關係的[範例](https://blog.safaribooksonline.com/2014/02/11/d3-js-maps/)

## Why need it?

為什麼我們會需要D3.js這樣圖像化地library勒？當我們處於一個大資料的時代，想要直接閱讀資料從中找出資訊是很不容易的事情，這是後我們可以透過圖像化的方式讓我們用看的就能感覺出他的重要性或差異，我們從下面這個範例來解說一下

## Setup and Demo

看完一串說明後，就讓我們來hand on一下吧

安裝上只要先include D3.js的js檔案

	<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>

接著就來個簡單的Select吧

<a class="jsbin-embed" href="http://jsbin.com/vokaz/3/embed?html,output">JS Bin</a><script src="http://static.jsbin.com/js/embed.js"></script>

就這樣透過select我們輕鬆地幫p元素換了顏色

剩下的我們就明天再繼續吧