---
layout: post
title: Visual Studio Code Icons Extension
subtitle: ''
author: Blackie
header-img: ''
sitemap: true
date: 2017-04-04 10:58:16
categories:
- Tool
tags:
- VSCode
---

使用vscode-icons 讓VSCode變得更一目瞭然

<!-- More -->

分享在VSCode上面不錯用的Extension - [vscode-icons](https://marketplace.visualstudio.com/items?itemName=robertohuertasm.vscode-icons)．它可以快速的幫我們將資料夾與檔案的副黨格式顯示出對應的icon圖示，讓我們可以在VSCode上面一路瞭然的知道目前現在操作的是哪一份格式的檔案．

在還沒安裝套件前我們的VSCode針對目錄與檔案的呈現是下面這樣的：

![before](before.png)

而當我們安裝完套件後並選定icon主題後(這邊以VSCode Icons為例)如下：

![after](after.png)

我們可以看到左邊的每個資料夾與檔案都有了圖示對應他的副檔名，而實際上這個也會出現在文件編輯時的tab上面讓我們更加清楚目前所在處．

而使用該套件要稍微注意一下，套件下載安裝完後預設的theme是none（這邊跟disable的效果會是一樣的)，所以我們要手動更新到一個指定的theme

![cmd](cmd.png)

而預設只有三種，如果還有其他需要也可以再從Extension Market上面下載後載入使用

![theme](theme.png)