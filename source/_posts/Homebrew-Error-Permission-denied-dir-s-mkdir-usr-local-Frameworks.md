---
title: 'Homebrew Error : Permission denied @ dir_s_mkdir - /usr/local/Frameworks'
sitemap: true
author: Blackie
categories:
  - Tool
tags:
  - Homebrew
date: 2021-09-12 15:24:31
---


紀錄一下當 OS 更新後遇到 Homebrew `Permission denied @ dir_s_mkdir - /usr/local/Frameworks` 的問題時該如何處理

![conver](cover.png)

<!-- More -->

最近在使用 Homebrew 會遇到無法安裝與更新的狀況：

![Homebrew - 按照指令更新](brew_update.png)

上述就算已經如描述更新 Git 後依舊會遇到問題．這邊 google 後發現可用 `brew doctor` 指令健檢與逐步修復，這邊趕快紀錄一下．

這邊主要是安裝時會遭遇以下錯誤：

![Homebrew - 更新失敗，有檔案權限不足](brew_error.png)

而透過`brew doctor` 指令可快速找到問題：

![使用 brew doctor](brew_doctor.png)

接著我們就是一步步按照指令修復即可
![修復 Homebrew](brew_doctor_fix.png)

## Ref

- [OMG！我的 Homebrew 🍺 壞掉了](https://medium.com/@theYenLai/omg-homebrew-fix-3b1e4f761ce7)