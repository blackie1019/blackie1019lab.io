---
title: Using App Scripts to batch Translation on Google Sheets
sitemap: true
author: Blackie
date: 2021-09-30 23:41:52
categories:
- Tool
tags:
- App Script
- Google Workspace
- Google Slide
---

介紹 App Scripts 如何增加

<!-- More -->

這邊以 Google Slide 為例，示範如何使用快速大量翻譯 [範例成果](https://docs.google.com/spreadsheets/d/1klAJLg6L1VWzrrpxnBgZq9hzsrVzwW8AZCRtvjsLOa4/edit?usp=sharing)	

## Basic : use GOOGLETRANSALTE function

[Official - GOOGLETRANSLATE](https://support.google.com/docs/answer/3093331?hl=zh-Hant) 是一個官方的翻譯函式，只要帶入指定轉換的語系，可以透過 `指定` 或是 `動態偵測` 的方式抓取原文語言進行大批量的翻譯．

![GOOGLETRANSLATE()進行翻譯](1.png)

![翻成英文(en)](2.png)

![翻成西班牙文(es-es)](3.png)

上面的翻譯都是單一欄位是同一指定語系時能寫死，但大多數情況其實輸入的內容都不知道、此時當然也不知道語系．

這時候我們就可以讓他自動偵測，這邊如果要使用自動偵測只需加入關鍵字 `auto` 作為源頭語系即可

![使用 auto 進行偵測](auto.png)

而我們也可以透過 [DETECTLANGUAGE](https://support.google.com/docs/answer/3093278) 函式進行指定的源頭語系偵測也是一種方式．

![DETECTLANGUAGE() 進行偵測](DETECTLANGUAGE.png)

## Advance : Use App Script

學會透過預設函式進行基本操作後，我們來演練如何撰寫 Google Translation 的 scripts 吧，這邊還不懂 App Scripts 的朋友可以先參考前篇基本教學 [sing App Scripts to easy create and publish add-ons for Google Sheets](https://blackie1019.gitlab.io/2021/09/28/Using-App-Scripts-to-easy-create-and-publish-add-ons-for-Google-Sheets/)

這邊最主要是呼叫 [LanguageApp.translate()](https://developers.google.com/apps-script/reference/language/language-app?hl=zh-tw) (但這邊官方範例也有西班牙文舉例就真的是恰巧了...)

```js
function translateToSpanish(input, originalLang) {
  return LanguageApp.translate(input, originalLang, 'es-es');
}
```

這邊比較有趣的部分是第二個參數其實不帶就可以自動偵測．

![呼叫程式碼](app_script.png)

## Ref

- [Quickstart: Add-on for Google Docs](https://developers.google.com/workspace/add-ons/editors/docs/quickstart/translate)
- [Language Translation Web App on Google Sheets](https://www.youtube.com/watch?v=BStykysPLx8)