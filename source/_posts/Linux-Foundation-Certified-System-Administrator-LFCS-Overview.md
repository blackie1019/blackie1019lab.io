---
title: >-
  Linux Foundation Certified System Administrator (LFCS) - Overview and
  Essential Commands
sitemap: true
author: Blackie
categories:
  - Linux
tags:
  - Linux Foundation Certified
  - LFCS
date: 2021-11-20 16:51:36
---

介紹 LFCS 的學習重點 - Overview and Essential Commands

<!-- More -->

![LFCS](lfcs.png)

## LFCS Introduction

Linux Foundation 針對 Linux 的認證考試，主要可以分成三種等級：

- LFCA : 初級、剛開始上手 Linux 環境
- LFCS : 中級、日常使用 Linux 環境與協助運行與維護
- LFCE : 專家級、主要靠 Linux 維護與調整賺錢

## Essential Commands

### Search for Files Part 1 - Find/Locate

- find : 找尋檔案

    find -name "123.txt"
    find / -name "123.txt"
    find /etc -iname "aBc.txt"
    find /etc -type d "d1"
- locat : TBD

### Search for Files Part 2 - Which/Whereis/Type

- which : 確認指令所執行的位置
- whereis : 確切的檔案位置

  ```
  whereis python | tr " " '\n'
  ```

- type : 執行路徑或是指令暱稱

### Evaluate and Compare the Basic File System Features and Options

![filesystem overview](02_filesystem_overview.png)

- block device : 儲存資料的實際物質
- fiesystem : 儲存資料的方法

- EXT4 ：是目前最佳且通用的
- XFS: Extents Filesystem, 很像 EXT4,  Can be expanded on the fly but can't be reduced
- FAT/332/exFAT: File Allocation Table Filesystem, exFAT 是大型檔案最佳選擇

### Compare and Manipulate File Content and Use Input-Output Redirection Part 1 - Create Files/Input-Output

![](02_create_manipulate.png)

- cat: 簡單讀出全部內容
- more: 讓output支援部分閱讀，可與 cat 結合
- less: 讓output支援上下滾動與查詢，可與 cat 結合
- sort: 讓output支援排序
- touch: 從 linux 指令寫入指定檔案, 可建立 zero byte file
- nano/vi: 從編輯器建立檔案

### Compare and Manipulate File Content and Use Input-Output Redirection Part 2 - Compare Files (Diff/Compare/CMP)

![Compare File](02_compare_file.png)

- diff: 可以列出兩邊檔案不同的地方
- comm: 如果內容有排序用這個比較比較有用，否則會出錯
- cmp：針對blob 進行比較並給出第一個差異的行數．

### Use Input-Output Redirection

- `|` : Pipe, 主要用於把 output 串連給其他指令進行操作
- `>` : Create/Overwrite, 覆寫當前指定檔案內容
- `>>` : Create/Append, 附加當前指定檔案內容
- `<`: Input, 主要用於帶入指令

/dev/null ：一個特殊的檔案，送過去的就會自動不見．

### Analyze Text Using Basic Regular Expressions

- grep

### Archive, Backup, Compress, Unpack, and Decompress Files

- tat -cvf ：建立 `.tar` 檔案
- tar -cvzf：建立用gzip壓縮的 `.tar.gz` 檔案
- tar -tf ：預覽但不解開 `.tar` 檔案
- tar -xvzf ：解壓縮 `.tar` 檔案

### Create, Delete, Copy, and Move Files and Directories

![Create, Delete, Copy and Move](02_create_delete_copy_move.png)

- touch file.txt
- nono/vi file.txt
- command > file.txt
- command >> file.txt

- mkdir

- rm
- rmdir
- rm -r
- rm -rf

- cp
- mv

### Create and Manage Hard and Soft Links

![hard and soft link](02_hard_soft_link.png)

- hardlink : 必須為同一個 filesystem 且只能是檔案．擁有相同的識別號碼. 砍掉原始檔案還會存在

  ```sh
  ln sample.txt samplehardlink
  ```

- softlink : 可以不用同一個 filesystem、同時支援檔案與目錄都可建立連結．擁有不同的識別號碼. 砍掉原始檔案連結就無法使用
  
  ```sh
  ln -s sample.txt samplesoftlink
  ```

### List, Set, and Change Standard File Permissions

![File Group and permission](02_file_group_user.png)

### Manage Access to the root Account

主要兩個指令：
- sudo => Superuser do
- su => Substitute

重點使用：

- id : 列出所有user id，能在 filesystem 中找到你所屬的 id 代表哪個 user
- cat /etc/sudoers : 設定 `sudo` 後能拿到的權限
- sudo : 取得在 `/etc/sudoers` 所提供的權限，僅於執行當下取得，每次指令執行後就回復原本權限與角色．
- su : 在登出前、本次身份暫時切換成 `root` 角色

lsattr
sudo chattr +i start.sh

find /opt/myapp -ls

find /opt/myapp -exec sudo chown cloud_user:devop {} \;
或是
sudo -i
find /opt/myapp -exec chown cloud_user:devop {} \;

針對不能執行的檔案
find /opt/myapp -name "d*" -ok chmod 660 {} \;

針對檔名不為d開頭但能執行的檔案
find /opt/myapp '!' -name "d*" -ok chmod 770 {} \;

find /home -nouser -nogroup -ls

### Read and Use System Documentation

- man : 基本如何使用指令介紹
- info : 詳細指令細節
- --help : 對於指令與更多可選參數有更詳細的介紹
- whatis : 簡單說明指令用途

### scp and rsync

```sh
# 遞迴檔案列出指定目錄下包含子目錄內所有展開
ls -lR /opt

# 將檔案遞迴傳送至指定位置
scp -rp /opt/myapp server2:/opt

# 將遠端檔案遞迴拉回
rsync -aP server2:/opt/myapi /opt

# 透過 ssh 直接單次執行展開目錄與子目錄指令
ssh server2 ls -lR /opt

# 同步比較一下遠端檔案內容差異
rsync -naP /opt/ server2:/opt
```
