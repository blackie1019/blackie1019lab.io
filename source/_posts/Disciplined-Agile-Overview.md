---
title: Disciplined Agile Overview
sitemap: true
author: Blackie
categories:
  - Management
tags:
  - Agile
  - Project Management
  - Disciplined Agile
date: 2021-10-19 13:05:36
---


![PMI - DA logo.png](pmi-da-logo.png)

鼎鼎大名的天才大師 ScottW. Ambler 和 Mark Lines 在 IBM 內部完成的，後轉移到美國項目管理協會（PMI），幾年的進一步更新和完善在 2020 有了一系列的推廣而火紅．今天稍微整理一下裡面的內容，幫助以後自我學習．

## Disciplined Agile Definition

>It is a process-decision toolkit that provides straightforward guidance to help people, teams, and organizations to streamline their processes in a context-sensitive manner.

Disciplined Agile(DA)，一般中文稱為`規範化的敏捷`．它其實是一個流程決策工具包，可提供直接的指導，幫助人員、團隊和組織以上下文相關的方式簡化流程。用以讓團隊以舒適的方式選擇自己合適的方式進行工作。不管團隊使用何種敏捷方法論如慣用LEAN、Scrum、或 Kanban 等方法的團隊，都能夠在DA找到一種工作方法，讓自己的工作更有彈性，幫助企業或是大型專案狀況，找出一種調整現狀就能走的新道路（這邊不是用改變，而是用調整）。

DA 建構了數個階段不同的生命週期，以及對應的工具，流程和思考邏輯，讓團隊有所依歸。

官方的 DA 流程圖(非制式，可依據團隊與實際流程調整、增加) :
![PMI - flex-workflow1.png](flex-workflow1.png)

但看了台灣一個敏捷教學的內容整理得更簡明扼要：
![PM 里長伯 - HLD DA Life Cycle](da-lifecycle.png)

而每個階段，都有需要達到的交付目標、用以衡量是否有達到這個週期的效益與評估下一期(Sprint)如何進行改善．

## DA Mindset and principles

DA 是由三個不同階段的規範逐步建立起基礎：

![PMI - mindset1.png](mindset1.png)

- 原則(Principles). The principles provide a philosophical foundation for business agility. They are based on both lean and flow concepts.
- 承諾(Promises). The promises are agreements that we make with our fellow teammates, our stakeholders, and other people within our organization whom we interact with. The promises define a collection of disciplined behaviours that enable us to collaborate effectively and professionally.
- 指南(Guidelines). These guidelines help us to be more effective in our way of working (WoW) and in improving our WoW over time.

原始資料參考：[PMI - The principles of the Disciplined Agile® (DA™)](https://www.pmi.org/disciplined-agile/mindset/mindset-principles)

## DA 重點

先回顧一下 DA Principles:

![PMI - The Seven DA Principles](The-Seven-DA-Principles.png)

這邊簡單總結給團隊、專案的一些實踐上的反饋：

1. 背景是很重要的（Context counts）：每個組織、專案與個體都不相同，沒有一個方法能直接套用，所以如何發揮當前項目、團隊的能力與瞭解他們的背景做選擇是很重要的
2. 可提供選項是很好的（Choice is good）：在每個流程、方法論中都有其優缺點，如何可以提供不同的選擇取其優點用在階段性狀態上是很重要的，已交付為目標用以持續強化組織、產品與團隊．而非強迫團隊轉變、轉型去適應方法
3. 實用的才是更佳的（Be Pragmatic）：因為組織、專案或是個體的獨特性與不同的是適應性等差異，所以能夠實作且帶來正面效果的才是好的方法，而非強調建立 "正確敏捷" 的前提要求團隊改變，因為每個團隊的正確都不相同．
4. 成為學習型組織以每天好（Get Better and Getting Better）：持續讓團隊、產品、成員能夠自我驅動、更新與尋求突破現況，而非乞求被救援或是期望單一方法論能只接拯救某專案項目或是團隊．

## Ref

- [YT - Disciplined Agile Overview](https://www.youtube.com/watch?v=dHNnYtCj5oc)
- [PM 里長伯碎碎念#13 Disciplined Agile Scrum Master證照](https://medium.com/pm%E9%87%8C%E9%95%B7%E4%BC%AF/pm-%E9%87%8C%E9%95%B7%E4%BC%AF%E7%A2%8E%E7%A2%8E%E5%BF%B5-13-disciplined-agile-scrum-master%E8%AD%89%E7%85%A7-2bc09aabf4e5)
- [Introduction to Disciplined Agile® (DA™)](https://www.pmi.org/disciplined-agile/introduction-to-disciplined-agile)
- [指导企业级敏捷的规范敏捷交付（DAD）](https://zhuanlan.zhihu.com/p/396997574)