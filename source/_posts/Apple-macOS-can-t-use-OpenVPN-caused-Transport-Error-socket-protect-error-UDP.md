---
title: >-
  Apple macOS can't use OpenVPN caused Transport Error: socket_protect
  error(UDP)
sitemap: true
author: Blackie
categories:
  - Tool
tags:
  - macOS
  - OpenVPN
date: 2024-07-31 08:53:46
---


當 macOS 遇上 OpenVPN 無法使用、且查看 log 後發現 `Transport Error: socket_protect error (UDP)` 該如何處理．

<!-- More -->

先前遇過幾次 macOS 使用 OpenVPN 無法連線．

![cannot_connect.png](cannot_connect.png)

狀況會是一直停留在連接的載入(轉圈圈...)，不管切換、重開機可能都無法修復．

此時如果點開右上角的 `client log` 可以看到如下方的錯誤資訊

![log.png](log.png)

查了一下官方 github 有蠻多 `apple silicon mac` 遇到類似問題(M1~M3都有遭遇)，而我手上的 M1、M3沒遇到，但 M2 在更新版本至 `Version 3.4.7 (4745)` 也是遇到同樣的 bug. 

按照討論串指引如果單次手動使用下方指令將服務以 `sudo` 方式開啟後再度啟動 OpenVPN 就可以正常使用

```shell
sudo /Library/Frameworks/OpenVPNConnect.framework/Versions/Current/usr/sbin/ovpnagent
```

此時 terminal 應該會持續出現 VPN代理透過 UDP 建立成功的資訊．

而當確認當次手動啟動可以生效後就要將剛剛的 shell script 改為啟動時自動加入，可以透過下方語法幫我們自動添加、確保開關機後還能自體修復：

```shell
sudo launchctl load -w /Library/LaunchDaemons/org.openvpn.client.plist
```

然後重開機測試一下有成功修復就可以繼續使用摟～

## Ref

- [OpenVPN/openvpn3 - bug for mac #139](https://github.com/OpenVPN/openvpn3/issues/139)