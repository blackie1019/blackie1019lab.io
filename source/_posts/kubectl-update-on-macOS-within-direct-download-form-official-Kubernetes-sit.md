---
title: kubectl update on macOS within direct download form official Kubernetes site
sitemap: true
author: Blackie
categories:
  - k8s
tags:
  - macOS
  - kubectl
date: 2024-02-14 22:08:15
---



隨手記錄一下 kubectl 於 macOS 安裝上的手法．

<!-- More -->

## 直接從 Kubernetes 的官方網站下載最新版本的 kubectl 並替換舊版本

1. 下載最新的 kubectl：

    用下面的命令下載適用於 macOS 的最新 kubectl 可執行檔案：

    ```bash
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/darwin/amd64/kubectl"
    ```

2. 提升權限讓下載的檔案可具備執行權限：

    ```bash
    chmod +x ./kubectl
    ```

3. 替換 kubectl 到你的 PATH（先前安裝的位置）:

    ```bash
    sudo mv ./kubectl /usr/local/bin/kubectl
    ```

    上面這步驟要確保 /usr/local/bin 在你當前電腦的 PATH 環境變數中。

4. 最後確認升級，檢查 kubectl 版本以確認升級成功。

    ```bash
    kubectl version --client
    ```

## 更簡易的方式 : 使用 Homebrew

在 macOS 上如果原先是透過 `Homebrew` 安裝的 kubectl，可以很容易地升級到最新版本。

1. 首先，確保 Homebrew 本身是最新的。所以先更新 Homebrew：

    ```bash
    brew update
    ```

2. 接下來，使用 Homebrew 升級 kubectl。

    ```bash
    brew upgrade kubectl
    ```

3. 升級後，一樣確認 kubectl 的版本以確保升級成功。

    ```bash
    kubectl version --client
    ```
