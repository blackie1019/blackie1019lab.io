---
title: Tunnel openVPN connection and proxy SOCKS5 with two layer/level proxy
sitemap: true
date: 2021-09-3 22:00:18
categories:
  - Cloud
tags:
  - OpenVPN
  - SOCKS5
  - GCP
  - Redsocks

---

介紹如何透過 `OpenVPN` , `Redsocks` 與 `SSH SOCKS5` 打造一個二級穿透的遠端代理(跳板)服務

<!-- More -->

## Background and System HLD

前兩篇分享了 [Using Redsocks and SOCKS5 as Proxy to connect internal Cloud SQL without public IP](https://blackie1019.gitlab.io/2021/08/31/Using-Redsocks-and-SOCKS5-as-Proxy-to-connect-internal-Cloud-SQL-without-public-IP/) 與 [Hosting OpenVPN on GCP](https://blackie1019.gitlab.io/2021/08/24/Hosting-OpenVPN-on-GCP/) 其主要的目的就是能建造一個二級穿透的遠端代理，讓小夥伴可以透過 VPN 就能存取原本應該受限制網站與 TCP 資源.

這邊整體的架構如下：

![架構全貌](overall.png)

一級代理就是 `vpn-hk` : 主要作為真實請求發送的代理端口
二級代理就是 `vpn-sg` : 主要作為一般終端用戶至代理主機的處理

而 一級代理 與 二級代理 中間則透過 `Redsocks` 將其兩者串接，達到安全性與防暴露．

## Prerequisite

複雜的安裝細節都已經在原先兩篇交代了，這邊只要回去參考即可．

- [Using Redsocks and SOCKS5 as Proxy to connect internal Cloud SQL without public IP](https://blackie1019.gitlab.io/2021/08/31/Using-Redsocks-and-SOCKS5-as-Proxy-to-connect-internal-Cloud-SQL-without-public-IP/)
- [Hosting OpenVPN on GCP](https://blackie1019.gitlab.io/2021/08/24/Hosting-OpenVPN-on-GCP/)

## Key setup!

這邊其實按照前兩篇設定完後只要改關鍵的 `vpn-sg` 的 iptables 的規則就好！

1. 首先我們先刪除 OpenVPN 這邊帶起的規則，預設是寫在 `POSTROUTING`
2. 多添加一條 `PREROUTING` 幫我們把指定的受限制資源規則放入，這邊以目標如果是 tcp 請求都要轉倒進入 `Redsocks` 的轉導規則作為範例：

```bash
sudo iptables -t nat -A PREROUTING -j REDSOCKS -p tcp -m comment --comment 'redsocks proxy'
# 這條規則是避免 OpenVPN 自我重導循環
sudo iptables -t nat -A POSTROUTING -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j MASQUERADE
```

最後記得將上述的修正寫成 `.sh` 並確保重開機時會載入執行即可（因為 OpenVPN 原先的安裝也會有一個 iptables 的重開機載入設定，要確保能夠正確複寫又不改動到原先的變更這邊使用疊加的方式處理）

## Common Commands

另外，這邊整理了架設過程會需要使用到的各種 linux 除錯與常用 command:

```bash
#透過關鍵字，查詢當前於背景運行的 process
ps aux | grep autossh

#查看註冊的 system 服務是否啟用
systemctl list-unit-files | grep enabled

#設定檔案擁有者與所屬組織
umask 0755, o=, g=

#根據『PATH』這個環境變數所規範的路徑，去搜尋『執行檔』的檔名
which autossh

#條列出 iptables 當前所有設定
sudo iptables -t nat -L -n --line-number

#刪除 iptables 內指定分類下的特定規則，下面指 OUTPUT 下的第6條規則
sudo iptables -t nat -D OUTPUT 6
```
