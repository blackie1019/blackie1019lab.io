---
layout: post
title: Visual Studio Code Insiders Build Introduction
subtitle: ''
author: Blackie
header-img: ''
sitemap: true
date: 2017-04-13 00:10:36
categories:
- Tool
tags:
- VSCode
---

介紹Visual Studio Code Insiders Build

<!-- More -->

在2016五月時發佈的Visual Studio Code Insiders Build其實就是一個開發者預覽版的概念，訴求就是因應社群的快速回應，微軟官方另外建立了一個版本，將每日發佈最新版的Visual Studio Code，並且如果遇到最新的release有問題時會退回到上一個穩定的版本做更新，並針對最新有問題的版本建立問題追蹤並快速修復．

![value_props](value_props.svg)

這樣的好處可以讓勇於接納新版的朋友(強者)可以最快的取得更新，並同時給予官方支援的肯定，省去自己拉新版建置後還要退版的問題．

而在安裝上，Visual Studio Code Insiders Build可與Visual Studio Code 並存:

![vscode_vscode_insiderbuild](vscode_vscode_insiderbuild.png)

而當我們有兩個VS Code時就可以用[Sync Setting](https://marketplace.visualstudio.com/items?itemName=Shan.code-settings-sync)這個extensions幫我們同步兩邊的主題, 套件與設定．

# References #

- [Evolution of the Insiders Build](https://code.visualstudio.com/blogs/2016/05/23/evolution-of-insiders) 