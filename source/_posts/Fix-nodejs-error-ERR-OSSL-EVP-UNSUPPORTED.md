---
title: 'Fix nodejs error :ERR_OSSL_EVP_UNSUPPORTED'
sitemap: true
author: Blackie
categories:
  - Development&Coding
tags:
  - NodeJS
  - OpenSSL
date: 2022-01-12 15:13:03
---


升級 nodejs v17 後遇到 `ERR_OSSL_EVP_UNSUPPORTED` 時的處理

<!--more-->

今天剛好升級了 nodejs v17 latest 版本(v17.3.1)後執行 React 的基本專案建置[建立全新的 React 應用程式](https://zh-hant.reactjs.org/docs/create-a-new-react-app.html)流程後遇到：

```sh
{
  opensslErrorStack: [ 'error:03000086:digital envelope routines::initialization error' ],
  library: 'digital envelope routines',
  reason: 'unsupported',
  code: 'ERR_OSSL_EVP_UNSUPPORTED'
}
```

詳細的錯誤：

![2.png](2.png)

先確認了一下當前本機的 nodejs 版本：

![1.png](1.png)

找了一下網上資訊，看來是有個功能預設被開啟了．

## 為什麼會拋出 ERR_OSSL_EVP_UNSUPPORTED

node.js v17 版本包含近期的 OpenSSL 3.0，根據 OpenSSL 的發布策略，OpenSSL 1.1.1 將在 2023-09-11 結束支持，此日期之前 Node.js v18 LTS 結束日期。

因為 OpenSSL 3.0 的算法和關鍵的時間對用戶的嚴格限制了對用戶的影響，為了在 SSL 7 保護下保護系統，Node.js v1 版本包含 OpenSSL 3.0 版本為的測試和留出的一個版本。

如果使用到的函式方法(method/function)是OpenSSL 3.0 算法默認不再允許的，那在 Node.js 17 之前的 Node 版本中，應用程序是可以正常運行的，但如果在 Node.js17 中將拋出一個錯誤代碼為 `ERR_OSSL_EVP_UNSUPPORTED` 的錯誤信息，也就是本文遇到的問題．

## 如何正確地避開

最簡單的方式只要添加環境變數去之支援舊版即可．

```sh
export NODE_OPTIONS=--openssl-legacy-provider
```

執行完後再度執行 `yarn start` 或是 `npm start` 就成功了．

但由於此項修改是改環境變數，所以每次新的 terminal 都需要先 export 此項環境變數．

更簡單一點的方式應該是讓 package.json 的設定指令內直接帶入環境變數也可以．

package.json - 修改前

```json
"scripts": {
  "start": "NODE_OPTIONS=--openssl-legacy-provider react-scripts start",
  "build": "react-scripts build",
  "test": "react-scripts test",
  "eject": "react-scripts eject"
}
```

package.json - 修改前

```json
"scripts": {
  "setEnv":"NODE_OPTIONS=--openssl-legacy-provider",
  "start": "npm run setEnv && react-scripts start",
  "build": "npm run setEnv && react-scripts build",
  "test": "npm run setEnv && react-scripts test",
  "eject": "npm run setEnv && react-scripts eject"
}
```

## Ref

- [Webpack build failing with ERR_OSSL_EVP_UNSUPPORTED](https://stackoverflow.com/questions/69394632/webpack-build-failing-with-err-ossl-evp-unsupported)