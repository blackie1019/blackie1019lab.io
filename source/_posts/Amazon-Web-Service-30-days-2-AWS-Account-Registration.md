---
layout: post
title: 'Amazon Web Service 30 days - 2: AWS Account Registration'
subtitle: ''
author: Blackie
header-img: ''
categories:
  - Cloud
tags:
  - AWS
sitemap: true
date: 2017-12-21 10:18:19
---

30天鐵人賽介紹 AWS 雲端世界 - 2: 申請與開通 AWS 帳號

<!-- More -->

第一天我們大概介紹了一下雲端服務觀與 AWS 大致上所提供的服務，今天我們來實際的註冊一個 AWS 帳號並開通我們帳號做使用。

## AWS Account Registeration ##

首先要跟大家說明的是 AWS 的帳號申請是包括內附12個月的免費使用額度(超過免費額度上限或是使用超過12個月才會收錢)，所以這邊是新使用雲端服務的朋友可以大膽地去申請。

而*Microsoft Azure*與*Google Cloud Platform*也是有一樣的免費額度與試用帳號，這邊對於其他雲端服務有興趣的朋友也可以一併嘗試比較。

### Account Information ###

而註冊使首先須至[建立 AWS 帳戶](https://portal.aws.amazon.com/billing/signup)頁面填妥資料。

![registeration_page.png](registeration_page.png)

![registeration_page_2.png](registeration_page_2.png)

### Payment Information ###

其中這部付款資訊是最重要的註冊時只會過卡不會扣款，除非你超過免費額度。而之後當免費額度用完或是超過時間或是額度則會從該綁訂的信用卡或是帳戶申請扣費。

![registeration_page_3.png](registeration_page_3.png)

### Phone Verification ###

接下來就是準備好電話接受帳號審核，這邊輸入電話號碼（手機或是市話都可以）系統會直接撥話進來。

![registeration_page_4.png](registeration_page_4.png)

當你在網頁按出送出時，系統會跳出一個四位數的識別碼(pincode)

![registeration_page_5.png](registeration_page_5.png)

接著就是接起電話後輸入頁面給你的識別碼。

![registeration_page_6.png](registeration_page_6.png)

![registeration_page_7.png](registeration_page_7.png)

然後就會看到成功的通過審核的畫面

![registeration_page_8.png](registeration_page_8.png)

### Support Plan ###

在最後一步驟會讓你選擇是否要買一些加值的客戶支援服務，這邊就要看各位的需求去申請了，但這邊的服務就是要額外收錢了，所以請斟酌使用。

![registeration_page_9.png](registeration_page_9.png)

### Registration Completed ###

當以上步驟都成功完成，代表你已經可以開始使用AWS的服務了。

![registeration_page_10.png](registeration_page_10.png)

到這邊我們第二天的帳戶申請就搞定了，明天我們還會花些時間解釋一下雲端服務上最重要的觀念 Global, Region, Available Zone 與 Edge。

讓我們繼續學下去吧！