---
title: NodeJS - nvm
sitemap: true
categories:
  - Cloud
tags:
  - NodeJS
  - nvm
date: 2021-09-04 00:24:27
---


使用 nvm 管理環境內多個版本的 NodeJS

<!-- More -->

## what is nvm?

NodeJS 一般新手安裝都是由[官方下載](https://nodejs.dev/download)一個安裝後於該環境執行，而對於比較真實的開發環境來說，由於往往會接觸到不同版本的 NodeJS, 所以一般開發會由 `nvm` 來幫我們做正確的多版本安裝才是比較實際的作法．

![官方 NodeJS 下載(不推薦用此方法安裝 NodeJS 環境)](node_download.png)

nvm(node version manger) 是 node 版本管理器。主要用於單一主機可執行與管理多個nodejs版本。可以快速實現node 安裝，卸載，與在不同版本切換而不用反覆地解除安裝與再安裝。而在 liunx/macOS 下如果感覺使用原生的 NodeJS 安裝包安裝過程比較繁瑣，也可以使用nvm安裝 NodeJS.

## nvm setup and basic use

透過上方一行指令

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
```

![macOS 上一鍵安裝 nvm](nvm_01.png)

之後就可以透過下方簡單指令進行安裝與版本切換

```bash
#安裝
nvm install 16

#卸載
nvm uninstall version

#切換node 版本至 14
nvm use version

#查看node 版本

node -v
```

![透過 nvm 安裝與切換 NodeJS 環境](nvm_02.png)

## Ref

- [https://github.com/nvm-sh/nvm](https://github.com/nvm-sh/nvm)
