---
title: Hello blackie1019.gitlab.io
author: Blackie
sitemap: true
date: 2021-07-15 19:47:38
---

職涯的新篇章

<!-- More -->

事隔兩年，又準備開始新的學習旅程了．

之前舊的 blog : [http://blackie1019.github.io/](http://blackie1019.github.io/) 留在 github 的也不刪除了，但未來就會以 gitlab 的為主開始發展下去．

一路上做軟體開發也超過十年了，有些心得、有些感觸、有著希望能持續進步的動力，所以開始這一篇新章．

2021 就從八月開始進步吧!