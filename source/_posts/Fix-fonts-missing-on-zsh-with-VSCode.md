---
title: Fix fonts missing on zsh with VSCode
date: 2021-08-08 18:30:06
sitemap: true
categories:
- Tool
tags:
- VSCode
- zsh
---

演示如何修復在使用 VSCode 時 zsh 的字型檔遺失問題

<!-- More -->

## 修福 zsh 的字型檔遺失

zsh 的安裝與使用應該是 linux 與 macOS 開發者拿到電腦的第一步．但往往安裝好酷炫的 theme 後就發現命令列於 VSCode 使用時會看到一推框框的字型遺失情況．

![1.png](1.png)

這邊修復方式很簡單，只需到 [https://github.com/powerline/fonts](https://github.com/powerline/fonts) 下載後依照指示逐步安裝遺失的字型檔案即可．

如果很懶得看設定的朋友可以先來一次 `./install.sh` 安裝全部．

![2.png](2.png)

這時候打開 VSCode 後應該還是會有缺字型的情況出現，原因是 VSCode 的命令列與命令列字型檔還需要額外調整

這邊可以參考下方兩項設定調整：

```json
"terminal.integrated.fontFamily": "Source Code Pro for Powerline",
"terminal.integrated.shell.osx": "/bin/zsh"
```

如果是 UI 畫面的話在下方紅匡處修改：

![3.jpg](3.jpg)

設定完後重啟 VSCode 內的命令列就可以發現變化了：

![4.png](4.png)

## 關於 bash 與 zsh

有些剛開始使用 maxOS 或是 Linux 開發的朋友往往電腦一到手不知道要更換預設的終端機，而一直用著 bash(沒有不好，只是還有更好的 zsh)．

這邊強烈建議拿到新電腦的朋友直接下載 [iterm2](https://iterm2.com/) 與 [oh my zsh](https://ohmyz.sh/)，可以參考這篇 [如何讓 Terminal 看起來好用又好看｜iTerms 2 + Oh-my-zsh 全攻略](https://medium.com/%E6%95%B8%E6%93%9A%E4%B8%8D%E6%AD%A2-not-only-data/macos-%E7%9A%84-terminal-%E5%A4%A7%E6%94%B9%E9%80%A0-iterms-oh-my-zsh-%E5%85%A8%E6%94%BB%E7%95%A5-77d5aae87b10)

另外，這邊引用 [關於 Linux 下 Bash 與 Zsh 啟動檔的載入順序研究](https://blog.miniasp.com/post/2021/07/26/Bash-and-Zsh-Initialization-Files) 讓不了解兩者設定檔案與其載入關係的朋友可以去參考一下這邊的整理．