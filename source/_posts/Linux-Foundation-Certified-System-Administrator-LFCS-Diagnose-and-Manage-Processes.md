---
title: >-
  Linux Foundation Certified System Administrator (LFCS) - Diagnose and Manage
  Processes
sitemap: true
author: Blackie
categories:
  - Linux
tags:
  - Linux Foundation Certified
  - LFCS
date: 2021-11-21 10:00:06
---


介紹 LFCS 的學習重點 - Diagnose and Manage Processes

<!-- More -->

## Boot, Reboot, and Shut Down a System Safely

![reboot](03_reboot.png)

- shutdown
- reboot
- halt
- poweroff

## Boot or Change System into Different Operating Modes

![runLevel](03_runlevel.png)

- 0 — Halt.
- 1 — Single-user text mode.
- 2 — Not used (user-definable).
- 3 — Full multi-user text mode.(3 means non-GUI mode).
- 4 — Not used (user-definable).
- 5 — Full multi-user graphical mode ( 5 means GUI ).
- 6 — Reboot.

[How to boot MacOS X, Linux , without GUI ?](https://theycallmeul.medium.com/how-to-boot-linux-macos-x-without-gui-469e9845dbae)

## Install, Configure, and Troubleshoot Boot-Loaders

From [https://zh.wikipedia.org/wiki/GNU_GRUB](https://zh.wikipedia.org/wiki/GNU_GRUB):
>GNU GRUB（簡稱「GRUB」）是一個來自GNU專案的啟動載入程式。GRUB是多啟動規範的實現，它允許使用者可以在電腦內同時擁有多個作業系統，並在電腦啟動時選擇希望執行的作業系統。GRUB可用於選擇作業系統分割區上的不同核心，也可用於向這些核心傳遞啟動參數。

>GNU GRUB的前身為Grand Unified Bootloader。它主要用於類Unix系統；同大多Linux發行版一樣，GNU系統也採用GNU GRUB作為它的啟動器。Solaris從10 1/06版開始在x86系統上也採用GNU GRUB作為啟動器。

Grub2為一來自GNU專案的啟動程式，可用來啟動任何硬體上的作業系統，它允許使用者在同一裝置上安裝多個作業系統，並在啟動時選擇所要執行的作業系統，而絕大多數的Linux版本都採用了Grub2

## Diagnose and Manage Processes

- top
- htop
- ps
- nice/renice : 在 Linux 中可以使用 `nice` 指令改變程式執行的優先權，但如果是執行到一半的程式要更改執行優先權的話，就沒辦法用 nice，這時候就可以使用 `renice` 這個指令。調整的數值：-20 最高, 20 最低，參考 [使用 renice 指令更改 Unix/Linux 上面程式執行的優先權（Scheduling Priority）](https://blog.gtwang.org/linux/renice-unixlinux-scheduling-priority/)

## Locate and Analyze System Log Files

在 `/var/log` 或是 `logs` 底下

Debian/Ubuntu Base:

- syslog
- auth.log

CentOs/Redhat Base:

- messages
- secure

## Schedule Tasks to Run at a Set Date and Time

![cron](03_cron.png)

- `crontab -l` ：顯示當前 crontab
- `crontab -e` : 編輯調整 crontab

## Update and Manage Software to Provide Required Functionality and Security, Part 1 - Ubuntu/Debian

- dpkg : `底層(Low-level)`介面，除非必要不建議預設使用
- aptitude : `高階(High-level)`介面，有更豐富的 UI 與功能
- apt/apt-get : `高階(High-level)`介面，建議預先使用此介面
  
## Update and Manage Software to Provide Required Functionality and Security, Part 2 - CentOS/Redhat

- rpm : 是`底層(Low-level)`安裝工具，除非必要不建議預設使用
- yum : 是`高階(High-level)`安裝工具, 舊版 CentOS/Redhat 的標準
- dnf : 是`高階(High-level)`安裝工具, 新版 Redhat8 的標準，大多指令與 yum 相同或類似

## Verify the Integrity and Availability of Resources

![Memory and Storage checking](03_memory_disk.png)

*DO NOT Check Mount System* 

如果在卸離(unmounting)前進行檢測，可能會導致檔案系統損毀

```sh

# 顯示當前 mount 的裝置
df -h 

# 卸離某一個指定裝置/設備
sudo unmount /path/to/fs
```

- fsck /path/to/fs

## Verify the Integrity and Availability of Key Processes

- ps
  - ps e ：所有
  - ps -ef : 更多資訊
  - ps aux ：所有執行中的
  - ps aux --forest : 顯示所有子執行緒及其關係

  STAT status:
  S: sleeping
  I: idle kernel
  N: nice
  l: multi-threaded
  s: session leader
  R: running
  T: stopped
  Z: zombie

- top ：即時監控
- htop ：更豐富且支援滑鼠互動的即時監控

## Change Kernel Runtime Parameters, Persistent and Non-Persistent

### Non-Persistent

1. sysctl
2. cd /proc/sys ls -al
3. sudo sysctl -w dev.cdrom.autoclose=0
4. cat autoclose

or 

1. sudo vi autoclose
2. modify to 1 and `wq`

### Persistent

- sysctl -p <.conf>
- edit and save configuration under /etc/sysctl.d
- sudo service procps start

## Use Scripting to Automate System Maintenance Tasks

```sh
#!/bin/bash
echo
echo "This is a bas script"
echo "User:"'whoami'
# lsb_release -d
# uname -or
# cat /proc/version
uname -or
echo
```

- chmod +u <filename.sh>

Two way:
1. ./<filename.sh>
2. add execute PATH to `export PATH=$PATH:<execute PATH>`
3. `echo PATH` to checking
4. execute filename.sh

## Scripting Conditionals and Loops Part 1 - Operators/If

- Arithmetic
  - addition : `+`
  - subtraction : `-`
  - multiplication : `*`
  - division : `/`
  - modulus : `%`
- Relational
  - `-eq`:等於
  - `-ne`:不等於
  - `-gt`:大於、不包含等於
  - `-lt`:小於、不包含等於
  - `-ge`:大於、包含等於
  - `-le`:小於、包含等於

Sample:

```sh
#!/bin/bash

echo
read -p "Enter A:" a
read -p "Enter B:" b
echo
echo "Results:"
echo

if [ $((a)) -eq $((b)) ]; then
        echo $((a)) -eq $((b))
fi

if [ $((a)) -ge $((b)) ]; then
        echo $((a)) -ge $((b))
fi

if [ $((a)) -gt $((b)) ]; then
        echo $((a)) -gt $((b))
fi

echo
```

## Scripting Conditionals and Loops Part 2 - For/While/Until

```sh
#!/bin/bash
for timer in 0 1 2 3 4 5
do
  echo "For loop Count up:" $timer
done

timer2=3
while [ $timer2 -gt 0 ]
do
  echo "While loop Count up:" $timer2
  timer2=$(($timer2-1))
done

timer3=3
until [ $timer3 -lt 1 ]
do
  echo "Until loop count up:" $timer3
  timer3=$(($timer3-1))
done
```

## Manage the Startup Process and Services (In Services Configuration)

![startup process status](03_startup_process.png)

### Check, Start and Stop

- systemctl status <service>
- systemctl start <service>
- systemctl stop <service>
- systemctl restart <service>

### Enable/Disable

- systemctl disable <service>
- systemctl enable <service>

## List and Identify SELinux/AppArmor File and Process Contexts

- ls -Z
- ps -auxZ

### SELinux

### AppArmor