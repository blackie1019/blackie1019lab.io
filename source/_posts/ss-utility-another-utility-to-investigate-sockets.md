---
title: ss utility - another utility to investigate sockets
author: Blackie
sitemap: true
date: 2021-09-11 14:56:37
categories:
- Tool
tags: 
- ss utility
---

介紹一款簡單易用的 TCP, UDP, Socket 連線檢查工具 - `ss utility`

<!-- More -->

netstat 指令已經淘汰了，現在大多用 `ss` 的指令取代 use netstat, ifconfig, ndp, arp, route 等網路指令，這邊簡單介紹安裝與操作

![ss utility 運行畫面](ss_cover.png)

## Install

### CentOS

```bash
sudo yum install iproute iproute-doc
```

### ubuntu

```bash
sudo apt-get install iproute2 iproute2-do
```

### macOS

因為官方沒有支援，所以這邊用非官方的版本 [https://github.com/brona/iproute2mac](https://github.com/brona/iproute2mac)

*macOS Big Sur 11.0.1* 之後的版本才可以安裝與使用

```bash
# Option 1. install from Homebrew
brew install iproute2mac

# Option 1. Manual install
curl --remote-name -L https://github.com/brona/iproute2mac/raw/master/src/ip.py
chmod +x ip.py
mv ip.py /usr/local/bin/ip
```

這邊稍微注意指令換成 `ip`, 而且參數也不太相同，用了一陣子後還是暫時擱著了．

## Usage Examples

這邊介紹幾個常用的範例，使用時可以搭配這邊準備的範例應用程序:

- 列出當前所有連線

  ```bash
  ss
  ```

- 列出所有與 `TCP` 協議有關連線的資訊

  ```bash
  ss -t -a
  ```

- 列出所有與 `UDP` 協議有關連線的資訊

  ```bash
  ss -u -a
  ```

- 列出所有連線相關的處理程序識別號(PID)

  ```bash
  ss -p
  ```

- 列出所有連線相關的統計資訊

  ```bash
  ss -s
  ```


## Options

參考 [ss(8) - Linux man page](https://linux.die.net/man/8/ss)

也可以透過 `MAN` 指令查詢:

```bash
man ss
```

- -h, --help
  Show summary of options.
- -V, --version
  Output version information.
- -n, --numeric
  Do now try to resolve service names.
- -r, --resolve
  Try to resolve numeric address/ports.
- -a, --all
  Display all sockets.
- -l, --listening
  Display listening sockets.
- -o, --options
  Show timer information.
- -e, --extended
  Show detailed socket information
- -m, --memory
  Show socket memory usage.
- -p, --processes
  Show process using socket.
- -i, --info
  Show internal TCP information.
- -s, --summary
  Print summary statistics. This option does not parse socket lists obtaining summary from various sources. It is useful when amount of sockets is so huge that parsing /proc/net/tcp is painful.
- -4, --ipv4
  Display only IP version 4 sockets (alias for -f inet).
- -6, --ipv6
  Display only IP version 6 sockets (alias for -f inet6).
- -0, --packet
  Display PACKET sockets.
- -t, --tcp
  Display only TCP sockets.
- -u, --udp
  Display only UDP sockets.
- -d, --dccp
  Display only DCCP sockets.
- -w, --raw
  Display only RAW sockets.
- -x, --unix
  Display only Unix domain sockets.
- -f FAMILY, --family=FAMILY
  Display sockets of type FAMILY. Currently the following families are supported: unix, inet, inet6, link, netlink.
- -A QUERY, --query=QUERY, --socket=QUERY
  List of socket tables to dump, separated by commas. The following identifiers are understood: all, inet, tcp, udp, raw, unix, packet, netlink, unix_dgram, unix_stream, packet_raw, packet_dgram.
- -D FILE, --diag=FILE
  Do not display anything, just dump raw information about TCP sockets to FILE after applying filters. If FILE is - stdout is used.
- -F FILE, --filter=FILE
  Read filter information from FILE. Each line of FILE is interpreted like single command line option. If FILE is - stdin is used.
- FILTER := [ state TCP-STATE ] [ EXPRESSION ]
  Please take a look at the official documentation (Debian package iproute-doc) for details regarding filters.

## Ref

- [SS Utility: Quick Intro](https://www.cyberciti.biz/files/ss.html)
- [The ss utility : Overview](https://codingbee.net/rhcsa/rhcsa-the-ss-utility)
- [How to Use the ss Command on Linux](https://www.howtogeek.com/681468/how-to-use-the-ss-command-on-linux/)