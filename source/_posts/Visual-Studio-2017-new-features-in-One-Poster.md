---
layout: post
title: Visual Studio 2017 new features in One Poster
subtitle: ''
author: Blackie
header-img: ''
sitemap: true
date: 2017-03-15 10:45:13
categories:
- Development&Coding
tags: 
- Visual Studio
---

隨著微軟新推出的Visual Studio 2017，讓我們用一張海報看完VS2017新的功能!

<!-- More -->

在3/7微軟最主要的產品Visual Studio迎接了它20歲的生日! 這邊官方也做了一個網頁來快速幫大家一覽所有新功能

![vs2017 poster](vsinfographic.png)

[來源](https://vs2017poster.azurewebsites.net/)

這邊會持續針對VS2017帶來的新功能做完整的解紹

## VS2017 New Features ##

- New Start page
- Open Folder
- Synchronize your environment
- Advanced IntelliSense
- Fast code navigation
- Live code analysis
- Connected services
- Xamarin Profiler

### Debugging & Support ###

- Run to click
- Exception Helper
- Make Visual Studio your own
- Live Unit Testing
- Docker support
- PowerShell Tools for Visual Studio
- Continuous Delivery Tools for Visual Studio

