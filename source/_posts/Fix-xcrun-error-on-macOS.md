---
title: Fix xcrun error on macOS
sitemap: true
categories:
  - Tool
tags:
  - macOS
date: 2021-08-27 18:25:18
---


在 macOS 遇到 `xcrun: error: invalid active developer path (/Library/Developer/CommandLineTools), missing xcrun at: /Library/Developer/CommandLineTools/usr/bin/xcrun` 問題時如何解決

<!-- More -->

每次當 macOS 升級時多多少少在開發或是使用application 都會遇到下面的錯誤訊息 `xcrun: error: invalid active developer path (/Library/Developer/CommandLineTools), missing xcrun at: /Library/Developer/CommandLineTools/usr/bin/xcrun`

這邊紀錄一下處理方式避免之後忘記：

這邊以 Rider 搭配 .NET6 時遇到

>Failed to sign apphost with error code 0: xcrun: error: invalid active developer path (/Library/Developer/CommandLineTools), missing xcrun at: /Library/Developer/CommandLineTools/usr/bin/xcrun
>			/Users/ct.tsai/Desktop/Repo/gitlab/asp-dotnet-core-6-spa-react/src/AspNeCore6WithReactSpa/Application.Web/obj/Debug/net6.0/apphost: the codesign_allocate helper tool cannot be found or used
>			at (536:5)

![Rider顯示無法編譯](xcrun_error_01.png)

直接於終端機執行更新 xcode 延伸套件：

```sh
xcode-select --install
```

![除了上述指令也可以直接打開 Xcode 更新](xcrun_error_02.png)

![等待下載完後會自動更新](xcrun_error_03.png)

![更新完成](xcrun_error_04.png)

安裝完成後接著回去重啟 IDE(以我這邊為例是 Rider, 即可成功編譯)