---
layout: post
title: Visual Studio Code Integrate with macOS Finder
subtitle: ''
author: Blackie
header-img: ''
sitemap: true
date: 2017-05-08 10:10:24
categories:
- Tool
tags:
- VSCode
- macOS
---

分享如何將VSCode 整合進入 Finder，提供快速開啟 *VSＣode* 的功能 

<!-- More -->

![cover](cover.png)

使用 macOS 的開發者一定都有一樣的問題：可以讓 VSCode 跟 Finder 更方便地打開資料夾或檔案嗎？今天就要分享這樣的一個小技巧．

這邊我們使用網路上熱心人士提供的解決辦法：

先至['open in' buttons for MacOS X Finder toolbar](https://github.com/cnstntn-kndrtv/open-in-buttons-for-finder-toolbar)下載整個專案．解壓所後可以看到下面的資料夾：

![folder](folder.png)

接著進入 app 資料夾然後按住 **⌘cmd** 並將 *Open in Code* 拖曳進 Finder 的 Toolbar 即可：

![instruction](instruction.png)

而以後要在 Finder 內開啟 VSCode 只要針對要開啟的檔案或資料夾選擇後按下該按鈕即可

<iframe width='560' height='315' src='https://www.youtube.com/embed/EC6Xhh1bEWc?ecver=2' frameborder='0' allowfullscreen></iframe> 

是不是簡單很多啊，趕快下載來使用吧！記得不要吝嗇到 Github 給作者回應感謝摟～